﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace CompressionDemo
{
    class Program
    {
        static void CompressFile(string inFilename, string outFilename)
        {
            StreamWriter writer = File.CreateText(@"C:\newfile.txt"); // as in a C++ it must exist
            inFilename = @"C:\newfile.txt";
            FileStream sourceFile = new FileStream(@"c:\newfile.txt", FileMode.Open); //https://msdn.microsoft.com/en-us/library/ms404280(v=vs.110).aspx
             //by opening the file specified in the inFilename
            outFilename = @"C:\outfile.txt"; // I have already created it
           FileStream destFile = File.Create("outFile.txt");//by creating a new file specified in the outFilename
            
            
             
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourceFile.ReadByte();
            while(theByte !=-1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
            writer.Close();
            destFile.Close();
            compStream.Close();
        }
        static void Main(string[] args)
        {
            CompressFile(@"C:\boot.ini", @"C:\boot.ini.gz");
        }
    }
}
