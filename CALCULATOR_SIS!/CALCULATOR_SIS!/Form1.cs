﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CALCULATOR_SIS_
{
    public partial class Form1 : Form
    {
        Double resultValue;
        String operationPerformed = "";
        bool isOperaionPerformed = false;

        public int minus = 1;
        public string str = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void button_click(object sender, EventArgs e)
        {
            if ((textBox_result.Text == "0") || (isOperaionPerformed))
            {
                textBox_result.Clear();
            }

            Button button = (Button)sender;
                isOperaionPerformed = false;
               

                if (button.Text == ".")
                {
                    if (!textBox_result.Text.Contains("."))
                    {
                        textBox_result.Text += button.Text;
                    }
                }
                else
                    textBox_result.Text += button.Text;
                

            
        }

        private void operation_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
           if(resultValue !=0)
           {
               if (button.Text == "sqrt")
               {
                 
                   textBox_result.Text = Operators.Sqrt(Double.Parse(textBox_result.Text)).ToString();
                   // textBox_result.Text = (Math.Sqrt((Double.Parse(textBox_result.Text)))).ToString();
              
               }
               equal.PerformClick();
                  
               isOperaionPerformed = true;
               operationPerformed = button.Text;
               labelCurrentOperation.Text = resultValue + " " + operationPerformed;
           }
           else if(button.Text == "sqrt")
           {
               textBox_result.Text = Operators.Sqrt(Double.Parse(textBox_result.Text)).ToString();
               resultValue = Operators.Sqrt(Double.Parse(textBox_result.Text));
           }
           else if(button.Text == "1/x")
               textBox_result.Text = (1 / Double.Parse(textBox_result.Text)).ToString();
           else if (button.Text == "%")
               textBox_result.Text = (Double.Parse(textBox_result.Text) / 100.0).ToString();
           else
           {
               operationPerformed = button.Text;
               resultValue = Double.Parse(textBox_result.Text);
               labelCurrentOperation.Text = resultValue + " " + operationPerformed;
               isOperaionPerformed = true;
           }
            
           
        }

        private void button11_Click(object sender, EventArgs e)
        {
            textBox_result.Text = "0";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox_result.Text = "0";
            resultValue = 0;
        }

        private void button16_Click(object sender, EventArgs e)
        {
            labelCurrentOperation.Text = "";
            isOperaionPerformed = false;    
            switch (operationPerformed)
            {
                case "+":
                    //textBox_result.Text = (resultValue + Double.Parse(textBox_result.Text)).ToString();
                    textBox_result.Text = Operators.Add(resultValue, Double.Parse(textBox_result.Text)).ToString();
                    break;
                case "-":
                    //textBox_result.Text = (resultValue - Double.Parse(textBox_result.Text)).ToString();
                    textBox_result.Text = Operators.Sub(resultValue, Double.Parse(textBox_result.Text)).ToString();
                    break;
                case "*":
                    //textBox_result.Text = (resultValue * Double.Parse(textBox_result.Text)).ToString();
                    textBox_result.Text = Operators.Mult(resultValue, Double.Parse(textBox_result.Text)).ToString();
                    break;
                case "/":
                    //textBox_result.Text = (resultValue / Double.Parse(textBox_result.Text)).ToString();
                    textBox_result.Text = Operators.Div(resultValue, Double.Parse(textBox_result.Text)).ToString();
                    break;

                default:
                    break;
            }
            resultValue = Double.Parse(textBox_result.Text);
            operationPerformed = "";
           
            labelCurrentOperation.Text = "";
        }

        private void labelCurrentOperation_Click(object sender, EventArgs e)
        {

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch(e.KeyChar.ToString())
            {
                case "1":
                    button_one.PerformClick();
                    break;
                case "2":
                    button_two.PerformClick();
                    break;
                case "3":
                    button_three.PerformClick();
                    break;
                case "4":
                    button_four.PerformClick();
                    break;
                case "5":
                    button_five.PerformClick();
                    break;
                case "6":
                    button_six.PerformClick();
                    break;
                case "7":
                    button_seven.PerformClick();
                    break;
                case "8":
                    button_eight.PerformClick();
                    break;
                case "9":
                    button_nine.PerformClick();
                    break;
                case "+":
                    addition.PerformClick();
                    break;
                case "-":
                   substract.PerformClick();
                    break;
                case "*":
                    multiply.PerformClick();
                    break;
                case "/":
                    divide.PerformClick();
                    break;
                case "=":
                    equal.PerformClick();
                    break;

                default:
                    break;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string removing = "";
            int k = textBox_result.Text.Length;
            for(int i=0; i<k; i++)
            {
                removing += textBox_result.Text[i];
                textBox_result.Text = removing;
                k--;

            }
            
            
        }
     
        private void sign_button(object sender, EventArgs e)
        {
            if (minus % 2 != 0)
            {
                str = textBox_result.Text;
                textBox_result.Text = ("-" + textBox_result.Text).ToString();
            }
            else
            {
                textBox_result.Clear();
                textBox_result.Text = str;
            }
            minus++;
        }

    


      

       
    }
}
