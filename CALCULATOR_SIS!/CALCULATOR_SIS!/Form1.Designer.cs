﻿namespace CALCULATOR_SIS_
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button_nine = new System.Windows.Forms.Button();
            this.divide = new System.Windows.Forms.Button();
            this.button_eight = new System.Windows.Forms.Button();
            this.button_seven = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button_six = new System.Windows.Forms.Button();
            this.multiply = new System.Windows.Forms.Button();
            this.button_five = new System.Windows.Forms.Button();
            this.button_four = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.addition = new System.Windows.Forms.Button();
            this.button_zero = new System.Windows.Forms.Button();
            this.button_three = new System.Windows.Forms.Button();
            this.substract = new System.Windows.Forms.Button();
            this.button_two = new System.Windows.Forms.Button();
            this.button_one = new System.Windows.Forms.Button();
            this.equal = new System.Windows.Forms.Button();
            this.textBox_result = new System.Windows.Forms.TextBox();
            this.labelCurrentOperation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(165, 162);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 31);
            this.button1.TabIndex = 0;
            this.button1.Text = "1/x";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.operation_click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(165, 125);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 31);
            this.button2.TabIndex = 1;
            this.button2.Text = "%";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.operation_click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(126, 88);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 31);
            this.button3.TabIndex = 2;
            this.button3.Text = "sqrt";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.operation_click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(165, 88);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 31);
            this.button4.TabIndex = 3;
            this.button4.Text = "<--";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(87, 88);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 31);
            this.button5.TabIndex = 4;
            this.button5.Text = "±";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.sign_button);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(48, 88);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(33, 31);
            this.button6.TabIndex = 9;
            this.button6.Text = "CE";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button_nine
            // 
            this.button_nine.Location = new System.Drawing.Point(87, 125);
            this.button_nine.Name = "button_nine";
            this.button_nine.Size = new System.Drawing.Size(33, 31);
            this.button_nine.TabIndex = 8;
            this.button_nine.Text = "9";
            this.button_nine.UseVisualStyleBackColor = true;
            this.button_nine.Click += new System.EventHandler(this.button_click);
            // 
            // divide
            // 
            this.divide.Location = new System.Drawing.Point(126, 125);
            this.divide.Name = "divide";
            this.divide.Size = new System.Drawing.Size(33, 31);
            this.divide.TabIndex = 7;
            this.divide.Text = "/";
            this.divide.UseVisualStyleBackColor = true;
            this.divide.Click += new System.EventHandler(this.operation_click);
            // 
            // button_eight
            // 
            this.button_eight.Location = new System.Drawing.Point(48, 125);
            this.button_eight.Name = "button_eight";
            this.button_eight.Size = new System.Drawing.Size(33, 31);
            this.button_eight.TabIndex = 6;
            this.button_eight.Text = "8";
            this.button_eight.UseVisualStyleBackColor = true;
            this.button_eight.Click += new System.EventHandler(this.button_click);
            // 
            // button_seven
            // 
            this.button_seven.Location = new System.Drawing.Point(9, 125);
            this.button_seven.Name = "button_seven";
            this.button_seven.Size = new System.Drawing.Size(33, 31);
            this.button_seven.TabIndex = 5;
            this.button_seven.Text = "7";
            this.button_seven.UseVisualStyleBackColor = true;
            this.button_seven.Click += new System.EventHandler(this.button_click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(9, 88);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(33, 31);
            this.button11.TabIndex = 14;
            this.button11.Text = "C";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button_six
            // 
            this.button_six.Location = new System.Drawing.Point(87, 162);
            this.button_six.Name = "button_six";
            this.button_six.Size = new System.Drawing.Size(33, 31);
            this.button_six.TabIndex = 13;
            this.button_six.Text = "6";
            this.button_six.UseVisualStyleBackColor = true;
            this.button_six.Click += new System.EventHandler(this.button_click);
            // 
            // multiply
            // 
            this.multiply.Location = new System.Drawing.Point(126, 162);
            this.multiply.Name = "multiply";
            this.multiply.Size = new System.Drawing.Size(33, 31);
            this.multiply.TabIndex = 12;
            this.multiply.Text = "*";
            this.multiply.UseVisualStyleBackColor = true;
            this.multiply.Click += new System.EventHandler(this.operation_click);
            // 
            // button_five
            // 
            this.button_five.Location = new System.Drawing.Point(48, 162);
            this.button_five.Name = "button_five";
            this.button_five.Size = new System.Drawing.Size(33, 31);
            this.button_five.TabIndex = 11;
            this.button_five.Text = "5";
            this.button_five.UseVisualStyleBackColor = true;
            this.button_five.Click += new System.EventHandler(this.button_click);
            // 
            // button_four
            // 
            this.button_four.Location = new System.Drawing.Point(9, 162);
            this.button_four.Name = "button_four";
            this.button_four.Size = new System.Drawing.Size(33, 31);
            this.button_four.TabIndex = 10;
            this.button_four.Text = "4";
            this.button_four.UseVisualStyleBackColor = true;
            this.button_four.Click += new System.EventHandler(this.button_click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(87, 238);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(33, 31);
            this.button17.TabIndex = 18;
            this.button17.Text = ".";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button_click);
            // 
            // addition
            // 
            this.addition.Location = new System.Drawing.Point(126, 238);
            this.addition.Name = "addition";
            this.addition.Size = new System.Drawing.Size(33, 31);
            this.addition.TabIndex = 17;
            this.addition.Text = "+";
            this.addition.UseVisualStyleBackColor = true;
            this.addition.Click += new System.EventHandler(this.operation_click);
            // 
            // button_zero
            // 
            this.button_zero.Location = new System.Drawing.Point(9, 238);
            this.button_zero.Name = "button_zero";
            this.button_zero.Size = new System.Drawing.Size(72, 31);
            this.button_zero.TabIndex = 15;
            this.button_zero.Text = "0";
            this.button_zero.UseVisualStyleBackColor = true;
            // 
            // button_three
            // 
            this.button_three.Location = new System.Drawing.Point(87, 199);
            this.button_three.Name = "button_three";
            this.button_three.Size = new System.Drawing.Size(33, 31);
            this.button_three.TabIndex = 23;
            this.button_three.Text = "3";
            this.button_three.UseVisualStyleBackColor = true;
            this.button_three.Click += new System.EventHandler(this.button_click);
            // 
            // substract
            // 
            this.substract.Location = new System.Drawing.Point(126, 199);
            this.substract.Name = "substract";
            this.substract.Size = new System.Drawing.Size(33, 31);
            this.substract.TabIndex = 22;
            this.substract.Text = "-";
            this.substract.UseVisualStyleBackColor = true;
            this.substract.Click += new System.EventHandler(this.operation_click);
            // 
            // button_two
            // 
            this.button_two.Location = new System.Drawing.Point(48, 199);
            this.button_two.Name = "button_two";
            this.button_two.Size = new System.Drawing.Size(33, 31);
            this.button_two.TabIndex = 21;
            this.button_two.Text = "2";
            this.button_two.UseVisualStyleBackColor = true;
            this.button_two.Click += new System.EventHandler(this.button_click);
            // 
            // button_one
            // 
            this.button_one.Location = new System.Drawing.Point(9, 199);
            this.button_one.Name = "button_one";
            this.button_one.Size = new System.Drawing.Size(33, 31);
            this.button_one.TabIndex = 20;
            this.button_one.Text = "1";
            this.button_one.UseVisualStyleBackColor = true;
            this.button_one.Click += new System.EventHandler(this.button_click);
            // 
            // equal
            // 
            this.equal.Location = new System.Drawing.Point(165, 199);
            this.equal.Name = "equal";
            this.equal.Size = new System.Drawing.Size(33, 70);
            this.equal.TabIndex = 24;
            this.equal.Text = "=";
            this.equal.UseVisualStyleBackColor = true;
            this.equal.Click += new System.EventHandler(this.button16_Click);
            // 
            // textBox_result
            // 
            this.textBox_result.Location = new System.Drawing.Point(9, 57);
            this.textBox_result.Multiline = true;
            this.textBox_result.Name = "textBox_result";
            this.textBox_result.Size = new System.Drawing.Size(189, 25);
            this.textBox_result.TabIndex = 25;
            this.textBox_result.Text = "0";
            this.textBox_result.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox_result.UseWaitCursor = true;
            // 
            // labelCurrentOperation
            // 
            this.labelCurrentOperation.AutoSize = true;
            this.labelCurrentOperation.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentOperation.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelCurrentOperation.Location = new System.Drawing.Point(12, 19);
            this.labelCurrentOperation.Name = "labelCurrentOperation";
            this.labelCurrentOperation.Size = new System.Drawing.Size(20, 18);
            this.labelCurrentOperation.TabIndex = 26;
            this.labelCurrentOperation.Text = "    ";
            this.labelCurrentOperation.Click += new System.EventHandler(this.labelCurrentOperation_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(207, 281);
            this.Controls.Add(this.labelCurrentOperation);
            this.Controls.Add(this.textBox_result);
            this.Controls.Add(this.equal);
            this.Controls.Add(this.button_three);
            this.Controls.Add(this.substract);
            this.Controls.Add(this.button_two);
            this.Controls.Add(this.button_one);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.addition);
            this.Controls.Add(this.button_zero);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button_six);
            this.Controls.Add(this.multiply);
            this.Controls.Add(this.button_five);
            this.Controls.Add(this.button_four);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button_nine);
            this.Controls.Add(this.divide);
            this.Controls.Add(this.button_eight);
            this.Controls.Add(this.button_seven);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator        ";
         
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button_nine;
        private System.Windows.Forms.Button divide;
        private System.Windows.Forms.Button button_eight;
        private System.Windows.Forms.Button button_seven;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button_six;
        private System.Windows.Forms.Button multiply;
        private System.Windows.Forms.Button button_five;
        private System.Windows.Forms.Button button_four;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button addition;
        private System.Windows.Forms.Button button_zero;
        private System.Windows.Forms.Button button_three;
        private System.Windows.Forms.Button substract;
        private System.Windows.Forms.Button button_two;
        private System.Windows.Forms.Button button_one;
        private System.Windows.Forms.Button equal;
        private System.Windows.Forms.TextBox textBox_result;
        private System.Windows.Forms.Label labelCurrentOperation;
    }
}

