﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIDTERM_TEST
{
   public class Point
    {

        public double x, y; //1.1 I have written double, because Math.Sqrt receives only double typed values
    
            //public Point(int x1=5, int x2=10, int y1=2, int y2=5)
            //{
            //    a = x1 + x2;
            //    b = y1 + y2;
            //}
            public override string ToString() //1.2
            {
                return x.ToString() + " " + y.ToString();
            }

            public static Point operator +(Point arg1, Point arg2) //1.3
            {
                arg1.x = arg1.x + arg2.x;
                arg1.y = arg1.y + arg2.y;
                return arg1;
            }
        
       

    }
}
